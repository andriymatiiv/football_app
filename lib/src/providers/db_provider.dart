import 'dart:io';

import 'package:football_data_app/src/model/fixture_league_model.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/model/league_model.dart';
import 'package:path/path.dart';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:catcher/catcher.dart';
import 'package:sqflite/sql.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();
  String creationScript1 = 'CREATE TABLE LEAGUE (id INTEGER PRIMARY KEY, '
      'name TEXT, country TEXT, logo TEXT, flag TEXT, round TEXT, season TEXT, datetime TEXT) ';
  String creationScript2 =
      'CREATE TABLE FIXTURE (id INTEGER PRIMARY KEY, fixtureData TEXT, datetime TEXT)';
  String creationScript3 =
      'CREATE TABLE Favourite (id INTEGER PRIMARY KEY, fixtureData TEXT, datetime TEXT)';
  String creationScript4 =
      "CREATE TABLE League (id INTEGER PRIMARY KEY, name TEXT, logo TEXT, country TEXT, flag TEXT, season INTEGER, start TEXT, end TEXT)";

  DBProvider._();

  Future<Database> get database async {
    if (_database != null)
      return _database;
    else {
      _database = await _initDB();
    }
    return _database;
  }

  _initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'football_data.db');

    return await openDatabase(path,
        version: 4, //TODO set version to 4
        onOpen: (db) {}, onCreate: (Database db, int version) async {
      //await db.execute(creationScript1);
      await db.execute(creationScript2);
      await db.execute(creationScript3);
      await db.execute(creationScript4);
    }, onUpgrade: (Database db, int oldVersion, int newVersion) async {
      if (newVersion == 4) {
        print("updated");
        await db.execute("DROP TABLE LEAGUE");
        await db.execute(creationScript4);
        //await db.execute("ALTER TABLE FIXTURE RENAME TO Fixture");
      }
    });
  }
  saveLeagues(List<League> leagues) async {
    final db = await database;
    db.rawDelete("DELETE FROM League");
    var batch = db.batch();
    for (var item in leagues) {
      batch.insert('League', item.toJson(), conflictAlgorithm: ConflictAlgorithm.replace);
    }
    await batch.commit(noResult: true);
  }

  saveFixtures(List<DBRecordModel> fixtures) async {
    final db = await database;
    await db.rawDelete("DELETE FROM Fixture");
    var batch = db.batch();
    for (var item in fixtures) {
      batch.insert('FIXTURE', item.toJson(), conflictAlgorithm: ConflictAlgorithm.replace);
    }
    await batch.commit(noResult: true).onError((error, stackTrace) {
      Catcher.reportCheckedError(error, stackTrace);
      return null;
    });
  }

  Future<bool> saveFavouriteTeam(DBRecordModel team) async {
    final db = await database;
    var queryResult =
        await db.rawQuery('SELECT * FROM Favourite WHERE id=${team.id}');
    if (queryResult.isEmpty) {
      db
          .insert('Favourite', team.toJson(),
              conflictAlgorithm: ConflictAlgorithm.replace)
          .onError((error, stackTrace) {
        Catcher.reportCheckedError(error, stackTrace);
        return 1;
      });
    }
    return true;
  }

  Future<int> deleteTeamFromFavourites(int teamId) async {
    final db = await database;
    final res = await db.rawDelete("DELETE FROM Favourite WHERE id = $teamId");
    return res;
  }

  Future<List<League>> getLeagues() async{
    final db = await database;
    String query = "SELECT * FROM League";
    final res = await db.rawQuery(query);
    List<League> list = res.isNotEmpty ? res.map((c) => League.fromJson(c)).toList() : [];
    return list;
  }
  Future<List<DBRecordModel>> getFixtures() async {
    final db = await database;
    String query = "SELECT * FROM Fixture";

    final res = await db.rawQuery(query);
    List<DBRecordModel> list = res.isNotEmpty ? res.map((c) => DBRecordModel.fromJson(c)).toList() : [];
    return list;
  }

  Future<List<DBRecordModel>> getFavouriteTeams() async {
    final db = await database;
    String query = "SELECT * FROM Favourite";
    final res = await db.rawQuery(query);
    List<DBRecordModel> list = res.isNotEmpty
        ? res.map((e) => DBRecordModel.fromJson(e)).toList()
        : [];
    return list;
  }

  Future<List<DBRecordModel>> getFixture(int fixtureId) async {
    final db = await database;
    String query = "SELECT * FROM Fixture WHERE id = $fixtureId";
    final res = await db.rawQuery(query);
    List<DBRecordModel> list = res.isNotEmpty
        ? res.map((c) => DBRecordModel.fromJson(c)).toList()
        : [];
    return list;
  }
  Future<bool> isActualDataStored(int periodInMinutes) async {
    bool isActual = false;
    final db = await database;
    int count = Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM Fixture'));
    if (count > 0) {
      var result =
          await db.rawQuery('SELECT * FROM Fixture ORDER BY id LIMIT 1');
      var fixture = result.isNotEmpty
          ? result.map((c) => DBRecordModel.fromJson(c)).toList().first
          : null;
      if (fixture != null) {
        var datetime = DateTime.parse(fixture.datetime);
        if ((DateTime.now()).difference(datetime).inMinutes <=
            periodInMinutes) {
          isActual = true;
        }
      }
    }
    return isActual;
  }

  Future<bool> isTeamPresent(int id) async {
    final db = await database;
    var res = Sqflite.firstIntValue(
        await db.rawQuery("SELECT COUNT(*) FROM Favourite WHERE id = $id"));
    if (res > 0) return true;
    return false;
  }
}
