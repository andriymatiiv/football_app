import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/model/fixture_league_model.dart';
import 'package:football_data_app/src/model/league_model.dart';
import 'package:football_data_app/src/model/team_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/db_provider.dart';

// ignore: camel_case_types
class DataProvider{
  DBProvider database;
  APIConnector connector;
  DataProvider({@required this.database, @required this.connector});

  Future<List<League>> getLeagues() async{
    List<League> leaguesData = await database.getLeagues();
    if(leaguesData.isNotEmpty){
      return leaguesData;
    }
    else{
      var leaguesData = await connector.getLeaguesData();
      database.saveLeagues(leaguesData);
    }
    leaguesData  = await database.getLeagues();
    return leaguesData;
  }
  Future<List<int>> getFixturesCount () async {
    List<int> counts =[];
    List<FixtureLeagueData> leagues = await getFixtureLeagues();
    List<Fixture> fixtures  = await getAllFixtures();
    leagues.forEach((league)  {
      var count =fixtures.where((element) => element.leagueId == league.id).toList().length;
      counts.add(count);
    });
    return counts;
  }
  reloadData(){
    connector.readAndSaveData();
  }
  Future<List<FixtureLeagueData>> getFixtureLeagues() async {
    List<DBRecordModel> dbFixtures = await database.getFixtures();
    List<FixtureLeagueData> leagues = [];
    dbFixtures.forEach((element) {
      var jsonData = json.decode(element.fixtureData);
      leagues.add(FixtureLeagueData.fromJson(jsonData["league"]));
    });
    final ids = leagues.map((e) => e.id).toSet();
    leagues.retainWhere((element) => ids.remove(element.id));
    return leagues;
  }
  Future<List<Fixture>> getAllFixtures() async{
      List<DBRecordModel> dbFixtures = await database.getFixtures();
      List<Fixture> fixtures = [];
      for(var item in dbFixtures){
        var jsonData = json.decode(item.fixtureData);
        fixtures.add(Fixture.fromJson(jsonData));
      }
      return fixtures;
    }
  Future<List<Fixture>> getFixtures(int leagueId) async{
    if(leagueId != null && leagueId>=0){
      List<DBRecordModel> dbFixtures = await database.getFixtures();
    List<Fixture> fixtures = [];
    for(var item in dbFixtures){
      var jsonData = json.decode(item.fixtureData);
      fixtures.add(Fixture.fromJson(jsonData));
    }
    List<Fixture> leagueFixtures =[];

      fixtures.forEach((element) {
        if(element.leagueId == leagueId){
          leagueFixtures.add(element);
        }
      });
      return leagueFixtures;
    }
    throw ArgumentError(" leagueId is null or less that zero");
  }
  Future<Fixture> getFixture(int fixtureId) async {
    Fixture fixture;
    bool isActual = await database.isActualDataStored(30);
    if(!isActual){
      await connector.readAndSaveData();
    }
    var result = await database.getFixture(fixtureId);
    fixture = Fixture.fromJson(json.decode(result.first.fixtureData));
    return fixture;
  }
  dynamic getFixtureLineups(int fixtureId) async{
    var response = await connector.loadFixtureLineups(fixtureId);
    return response;
  }

  Future<void> addTeam(int id) async{
    var responce = await connector.getTeamInfo(id);
    DBRecordModel model = DBRecordModel(id: id,
        fixtureData: json.encode(responce),
        datetime: DateTime.now().toIso8601String()
    );
    database.saveFavouriteTeam(model);
  }
  Future<List<Team>> getFavouriteTeams() async {
    List<DBRecordModel> teamsData = await database.getFavouriteTeams();
    List<Team> teams =[];
    if(teamsData != null)
    teamsData.forEach((element) {
      var jsonData = json.decode(element.fixtureData);
      teams.add(Team.fromJson(jsonData[0]["team"]));
    });
    return teams;
  }
  void removeFromFavourite(int teamId){
      database.deleteTeamFromFavourites(teamId);
  }
  Future<bool> isFavouriteTeam(int teamId) async {
    bool isPresent = await database.isTeamPresent(teamId);
    return isPresent;
  }

  Future<List<Fixture>> getTeamFixtures(int teamId) async{
      var inputfixtures = await connector.getTeamFixtures(teamId);
      List<Fixture> fixtures = [];
      inputfixtures.forEach((element) {fixtures.add(Fixture.fromJson(element)); });
      return fixtures;
  }
  Future<List<Fixture>> getActiveFixtures() async {
    var dbfixtures = await database.getFixtures();
    List<Fixture> fixtures = [];
    dbfixtures.forEach((element) => fixtures.add(Fixture.fromJson(json.decode(element.fixtureData))));
    List<Fixture> activeFixtures = [];
    fixtures.forEach((element) {
      if(element.status["elapsed"] != null && (element.status["short"] != "FT" && element.status["short"] != "AET" && element.status["short"] != "PEN")){
        activeFixtures.add(element);
      }
    });
    return activeFixtures;
  }
  Future<dynamic> getStandings (int LeagueId) async{
    var data = await connector.getStandingData(LeagueId);
    return data[0]["league"];
  }
}