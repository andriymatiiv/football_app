import 'dart:io';
import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/views/app.dart';
import 'file:///D:/test/Curs3/3.2/MobileDev/football_data_app/lib/main.dart';
import 'dart:math';

class ServiceRoutine{
  APIConnector connector;
  //NotificationDetails notificationDetails;
  ServiceRoutine({@required this.connector});
  Future<bool> executeTask() async {
    bool isOk=false;
    if(connector != null){
      bool isOk = await connector.readAndSaveData();
      int id= Random().nextInt(100);
      if(isOk)
        showNotification(id, "Data updating", "Data was updated in background", payload: "1");
      else
        showNotification(id, "Data updating", "Some error ocurred", payload: "1");
      //flutterLocalNotificationsPlugin.cancel(id);
    }else{
      Catcher.reportCheckedError(Exception("No connector provided"), StackTrace.current);
    }
    return isOk;
  }
  void initializeNotifications() async {
    AndroidInitializationSettings androidInitializationSettings = AndroidInitializationSettings('app_icon');
    InitializationSettings initializationSettings = InitializationSettings(android: androidInitializationSettings);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: onSelectNotification);
  }
  Future onSelectNotification(String payload) async{
    if(payload != null){
      /*int fixtureId = int.tryParse(payload);
      if(fixtureId != null){
        //Navigator.push(this.context, );
      }*/
      navKey.currentState.push(MaterialPageRoute(builder: (context) => App()));
    }
    //
  }
  /*Future<void> initializeNotifications() async {

  }*/
  showNotification(int id, String title, String subtitle, {String payload}) async {
    AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
        'Channel ID', 'Channel title', 'channel body',
        priority: Priority.defaultPriority,
        importance: Importance.high,
        showWhen: false);
    IOSNotificationDetails iosNotificationDetails = IOSNotificationDetails();
    NotificationDetails notificationDetails = NotificationDetails(
        android: androidNotificationDetails,
        iOS: iosNotificationDetails
    );
    await flutterLocalNotificationsPlugin.show(
        id, title, subtitle, notificationDetails,
    payload: payload);
  }
}