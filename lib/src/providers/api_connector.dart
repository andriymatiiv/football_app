import 'package:catcher/catcher.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import "dart:convert";
import 'package:intl/intl.dart';

import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/model/league_model.dart';
class APIConnector {
  static const _api_key = 'd24e4c26e2msh607ebb1fd0d2b75p188520jsnf6f8b27df332';
  static const String _baseUrl = "api-football-v1.p.rapidapi.com";
  static const Map<String, String> _headers = {
    "x-rapidapi-host": "api-football-v1.p.rapidapi.com",
    "x-rapidapi-key": _api_key,
  };

  Future<dynamic> getDataFromAPI({@required String endpoint, @required Map<String,String> query}) async {
    Uri uri = Uri.https(_baseUrl, endpoint, query);
    final response = await http.get(uri, headers: _headers);
    if(response.statusCode == 200){
      return json.decode(response.body) as Map;
    }else{
      throw Exception("failed to read data");
    }
  }
  List<DBRecordModel> getFixturesData(var jsonData){
    List<DBRecordModel> fixtures = [];
    var rawFixturesData = jsonData["response"] as List;

    rawFixturesData.forEach((element) {
     fixtures.add(DBRecordModel(id: element["fixture"]["id"],
     fixtureData: json.encode(element),
       datetime: DateTime.now().toIso8601String()
     ));
    });
    return fixtures;
  }
  Future<bool> readAndSaveData() async{
      final DateFormat formatter = DateFormat('yyyy-MM-dd');
      final currDate = DateTime.now();
      final Map <String, String> params = {
        "date" : formatter.format(currDate),
      };
      try{
        var returnedData = await getDataFromAPI(endpoint: "v3/fixtures", query: params);
        DBProvider.db.saveFixtures(getFixturesData(returnedData));
      }
      catch  (e){
        Catcher.reportCheckedError(e, StackTrace.current);
        return false;
      }
      return true;
  }
  Future<dynamic> loadFixtureLineups(int fixtureId) async{
    Map<String, String> params = {
      "fixture" : fixtureId.toString(),
    };
    var APIresponse = await getDataFromAPI(endpoint: "v3/fixtures/lineups", query: params);
    var responseData = APIresponse["response"] as List;
    return responseData;
  }
  Future<dynamic> getTeamInfo(int teamId) async{
    Map<String, String> params = {
      "id" : teamId.toString(),
    };
    var APIresponse = await getDataFromAPI(endpoint: "v3/teams", query: params);
    var responseData = APIresponse["response"];
    return responseData;
  }
  Future<dynamic> getTeamFixtures(int teamId) async{
    Map<String, String> params = {
      "team" : teamId.toString(),
      "season" : 2020.toString()
    };
    var APIresponse = await getDataFromAPI(endpoint: "v3/fixtures", query: params);
    var responseData = APIresponse["response"] as List;
    return responseData;
  }
  Future<List<League>> getLeaguesData() async{
    Map<String, String> params = {
      "season" : 2020.toString()
    };
    var APIresponse = await getDataFromAPI(endpoint: "v3/leagues", query: params);
    var responseData = APIresponse["response"] as List;
    List<League> leaguesData = [];
    responseData.forEach((jsonData) {
      leaguesData.add(League(
        id: jsonData["league"]["id"],
        name: jsonData["league"]["name"],
        logo:  jsonData["league"]["logo"],
        country: jsonData["country"]["name"],
        flag:  jsonData["country"]["flag"],
        season: jsonData["seasons"][0]["year"],
        start: jsonData["seasons"][0]["start"],
        end: jsonData["seasons"][0]["end"],
      ));
    });
    //DBProvider.db.saveLeagues(leaguesData);
    return leaguesData;
  }
  Future<dynamic> getStandingData(int leagueId) async {
    Map<String, String> params = {
      "season" : 2020.toString(),
      "league" : leagueId.toString(),
    };
    var APIresponse = await getDataFromAPI(endpoint: "v3/standings", query: params);
    var responseData = APIresponse["response"] as List;
    return responseData;
  }
}