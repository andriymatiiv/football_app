import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/views/custom_tile.dart';
import 'package:intl/intl.dart';
class LeagueFixturesWidget extends StatefulWidget {
  final int leagueId;
  final String leagueName;
  final DataProvider reader;
  final Function(int, String, Fixture) onPush;
  LeagueFixturesWidget({Key key, @required this.leagueId, @required this.leagueName, @required this.reader, @required this.onPush}) : super(key: key);
  @override
  _LeagueFixturesWidgetState createState() => _LeagueFixturesWidgetState();
}

class _LeagueFixturesWidgetState extends State<LeagueFixturesWidget> {
  APIConnector connector = new APIConnector();
  bool isLoaded = false;
  List<Fixture> fixtures;
  @override
  void initState() {
    super.initState();
    /*() async {
      var loadedFixtures = await widget.reader.getFixtures(widget.leagueId);
      setState(() {
        isLoaded = true;
        fixtures = loadedFixtures;
      });
    }();*/
    loadData();
  }
  @override
  Widget build(BuildContext context) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
    return Scaffold(
      appBar: AppBar(
        title: Column(
          children: [
            Text(widget.leagueName),
            Padding(
              padding: EdgeInsets.all(2),
              child:Text(formatter.format(DateTime.now()),
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.normal
                ),
              ),
            ),
          ],
        ),
        centerTitle: true,
      ),
      body: (){
        if(!isLoaded)
          return Center(child: CircularProgressIndicator());
        else{
          if(fixtures == null || fixtures.length == 0)
            return Center(child: Text("No data available"));
          return RefreshIndicator(
              child: ListView.builder(
              itemCount: fixtures.length *2,
              itemBuilder: (BuildContext context, int index) {
                if(index.isOdd)
                  return Divider();
                int i = index ~/2;
                return CustomListTile(
                  fixture: fixtures[i],
                  onTapCallback: (){
                    widget.onPush(fixtures[i].id, null, fixtures[i]);
                  },
                );
              }
          ),
              onRefresh: loadData);


          }
      }()
    );
  }
  Future<void> loadData() async {
    var loadedFixtures = await widget.reader.getFixtures(widget.leagueId);
    setState(() {
      isLoaded = true;
      fixtures = loadedFixtures;
    });
  }
}
