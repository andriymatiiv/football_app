import 'package:flutter/material.dart';
import 'package:football_data_app/src/navigators/live_tab_navigator.dart';
import 'package:football_data_app/src/navigators/standings_tab_navigator.dart';
import 'package:football_data_app/src/views/bottom_navigation.dart';
import 'package:football_data_app/src/navigators/favourites_tab_navigator.dart';

import '../navigators/main_tab_navigator.dart';
enum PageId {main, live, favourite, table}

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with SingleTickerProviderStateMixin{
  int index = 0;
  final navigatorKey = GlobalKey<NavigatorState>();
  PageId _currentPage = PageId.main;
  final _navigatorKeys= {
    PageId.main: GlobalKey<NavigatorState>(), //fixtures
    PageId.live : GlobalKey<NavigatorState>(), // live
    PageId.favourite : GlobalKey<NavigatorState>(),
    PageId.table : GlobalKey<NavigatorState>(), //TODO implement page
  };
  void _selectTab(PageId pageId){
    if(_currentPage == pageId){
      _navigatorKeys[pageId].currentState.popUntil((route) => route.isFirst);
    }else{
      int index = 0;
      switch (pageId){
        case (PageId.main) : { index = 0;} break;
        case (PageId.live) : index = 1; break;
        case (PageId.favourite) : index = 2; break;
        case (PageId.table) : index = 3;  break;
      }
      setState(() {
        _currentPage = pageId;
        //_tabController.index = index;
      });
    }
  }
  /*List<Widget> _tabs;
  TabController _tabController;
  List<BuildContext> navStack = [null, null];*/

  /*@override void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this);
    _tabs = <Widget> [
      MainPageTabNavigator(navigatorKey: _navigatorKeys[PageId.main]),
      LivePageNavigator(navigatorKey: _navigatorKeys[PageId.live]),
      FavouritesPageNavigator(navigatorKey: _navigatorKeys[PageId.favourite]),
      StandingsTabNavigator(navigatorKey: _navigatorKeys[PageId.table])
    ];
  }*/
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => !await _navigatorKeys[_currentPage].currentState.maybePop(),
        child: Scaffold(
          body: /*TabBarView(
            controller: _tabController,
            physics: NeverScrollableScrollPhysics(),
            children: _tabs,
          ),*/Stack(
            children: <Widget>[
                _buildNavigator(PageId.main),
              _buildNavigator(PageId.live),
              _buildNavigator(PageId.favourite),
              _buildNavigator(PageId.table),
            ],
          ),
          bottomNavigationBar: OwnBottomNavigationBar(
            currentPage: _currentPage,
            pageSelected: _selectTab,
          ),
        ),

    );
  }
  Widget _buildNavigator(PageId pageId){
    Widget child;

    switch (pageId){
      case PageId.main: { child = MainPageTabNavigator(navigatorKey: _navigatorKeys[PageId.main]);} break;
      case PageId.live: { child = LivePageNavigator(navigatorKey: _navigatorKeys[PageId.live]);} break;
      case PageId.favourite: { child = FavouritesPageNavigator(navigatorKey: _navigatorKeys[PageId.favourite]);} break;
      case PageId.table: { child = StandingsTabNavigator(navigatorKey: _navigatorKeys[PageId.table]);} break;
    }
    return Offstage(
      offstage: _currentPage != pageId,
      child: child,
    );
  }
}
