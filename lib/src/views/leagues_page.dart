import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/league_model.dart';
import 'package:football_data_app/src/providers/data_provider.dart';

class LeaguesPage extends StatefulWidget {
  final Function (String, int) onPush;
  final DataProvider provider;
  LeaguesPage({Key key, @required this.onPush, @required this.provider}) : super(key: key);
  @override
  _LeaguesPageState createState() => _LeaguesPageState();
}

class _LeaguesPageState extends State<LeaguesPage> {
  List<League> leagues;
  bool isLoaded = false;
  @override
  void initState() {
    super.initState();
    () async {
      await Future.delayed(Duration(seconds: 2));
      var response = await widget.provider.getLeagues();
      setState(() {
        leagues = response;
        isLoaded = true;
      });
    }();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Leagues"),
        centerTitle: true,
      ),
      body: (){
        if(!isLoaded)
          return Center(
            child: CircularProgressIndicator(),
          );
        if(leagues == null){
          return Center(child: Text("No data available"));
        }
        return ListView.builder(
          itemCount: leagues != null? leagues.length*2 : 0,
            //itemExtent: 30,
            itemBuilder: (BuildContext context, int index){
              if(index.isOdd)
                return Divider();
              int i = index ~/2;
              var league = leagues[i];
              return ListTile(
                leading: getIcon(league.logo),
                title: Text(league.country + ":  "+ league.name),
                contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
                //dense: true,
                onTap: () {widget.onPush("/standings", league.id);},
              );
            });
      }(),

    );
  }

  Widget getIcon(String path) {
    return CachedNetworkImage(
      imageUrl: path,
      width: 25,
      height: 25,
      errorWidget: (context, path, error) => Icon(Icons.error),
    );
  }
}
