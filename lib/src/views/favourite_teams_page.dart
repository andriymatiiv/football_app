import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/team_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/navigators/favourites_tab_navigator.dart';

class FavouriteTeamsPage extends StatefulWidget {
  FavouriteTeamsPage({Key key, @required this.onPush}) : super(key: key);
  final Function(int, RoutesEnum) onPush;
  @override
  _FavouriteTeamsPageState createState() => _FavouriteTeamsPageState();
}

class _FavouriteTeamsPageState extends State<FavouriteTeamsPage> {
  List<Team> _favouriteTeams;
  DataProvider _dataProvider;

  @override
  void initState() {
    _dataProvider = DataProvider(database: DBProvider.db, connector: APIConnector());
    getFavouriteTeams();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Favourite teams"),
        centerTitle: true,
      ),
      body: RefreshIndicator(
        onRefresh: getFavouriteTeams,
        child: ListView.builder(
          padding: const EdgeInsets.all(10),
          //itemExtent: 30,
          itemCount: _favouriteTeams!= null ? _favouriteTeams.length*2 : 0,
          itemBuilder: (BuildContext context, int index) {
            if (index.isOdd) return Divider();
            int i = index ~/ 2;
            return _getTile(context, _favouriteTeams[i]);
          },
        ),
      ),
    );
  }

  Future<void> getFavouriteTeams() async {
    var teams = await _dataProvider.getFavouriteTeams();
    if (teams != null) {
      setState(() {
        _favouriteTeams = teams;
      });
    }
  }

  ListTile _getTile(BuildContext context, Team team) {
    if (team == null) return null;
    return ListTile(
      leading: getIcon(team.logo),
      title: Text(team.name),
      trailing: IconButton(
        icon: Icon(Icons.favorite),
        onPressed: () => showAlertDialog(context, team.id),
      ),
      onTap: () {
        widget.onPush(team.id, RoutesEnum.fixturesRoad);
      },
      //dense: true,
      //contentPadding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 10),
    );
  }
 showAlertDialog (BuildContext context, int id){
   Widget cancelButton = TextButton(
     child: Text("Cancel"),
     onPressed:  () {
       Navigator.of(context, rootNavigator: true).pop('dialog');
     },
   );
   Widget continueButton = TextButton(
     child: Text("Delete"),
     onPressed:  () {
       Navigator.of(context, rootNavigator: true).pop('dialog');
       removeFromFavourite(id);
     },
   );
   AlertDialog alert = AlertDialog(
     title: Text("Delete from favourite"),
     content: Text("Would you like to delete team from favourites?"),
     actions: [
       cancelButton,
       continueButton,
     ],
   );
   showDialog(
     context: context,
     builder: (BuildContext context) {
       return alert;
     },
   );
 }
  Widget getIcon(String path) {
    var connection = new Connectivity().checkConnectivity();
    if (!(connection == ConnectivityResult.wifi ||
        connection == ConnectivityResult.mobile))
      return CachedNetworkImage(
          imageUrl: path,
          errorWidget: (context, path, error) => Icon(Icons.error),
          width: 20,
          height: 20);
    else
      return Icon(
        Icons.image_outlined,
        color: Colors.black26,
        size: 20,
      );
  }
  void removeFromFavourite(int id){
    _dataProvider.removeFromFavourite(id);
    setState(() {
      _favouriteTeams.removeWhere((element) => element.id == id);
    });
  }
}
