import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/views/custom_tile.dart';
import 'package:football_data_app/src/navigators/favourites_tab_navigator.dart';
class TeamFixturesPage extends StatefulWidget {
  final int teamId;
  final DataProvider provider;
  final Function(Fixture, RoutesEnum, int) onPush;
  TeamFixturesPage({Key key, @required this.teamId, @required this.provider, @required this.onPush}) : super(key: key);
  @override
  _TeamFixturesPageState createState() => _TeamFixturesPageState();
}

class _TeamFixturesPageState extends State<TeamFixturesPage> {
  List<Fixture> teamFixtures = [];
  @override
  void initState() {
    super.initState();
    () async {
      await Future.delayed(Duration(seconds: 1));
      var fixtures= await widget.provider.getTeamFixtures(widget.teamId);
      if(fixtures != null )
        if(fixtures.length > 0)
          setState(() {
            teamFixtures = fixtures;
          });
    }();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Team fixtures"),
        centerTitle: true,
      ),
      body: () {
        if(teamFixtures != null){
          return ListView.builder(
              itemCount: teamFixtures.length*2,
              itemBuilder: (BuildContext context, int index) {
                if(index.isOdd)
                  return Divider();
                int i = index ~/2;
                return CustomListTile(
                  fixture: teamFixtures[i],
                  onTapCallback: () {
                    widget.onPush(teamFixtures[i], RoutesEnum.fixtureRoad, teamFixtures[i].id);
                  },
                );
              },
          );
        }
          else return Center(
          child: Text("No fixtures for this team"),
        );
      }(),

    );
  }
}
