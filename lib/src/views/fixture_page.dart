import 'dart:typed_data';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/views/standings_page.dart';
import 'package:intl/intl.dart';
import 'dart:io';

import 'package:share/share.dart';
import 'package:screenshot/screenshot.dart';
import 'package:path_provider/path_provider.dart';
import 'package:catcher/catcher.dart';

class FixtureInfo extends StatefulWidget {
  //final int id;
  final Fixture fixture;

  FixtureInfo({Key key, @required this.fixture}) : super(key: key);

  @override
  _FixtureInfoState createState() => _FixtureInfoState();
}

class _FixtureInfoState extends State<FixtureInfo> {
  //ShortFixture fixture;
  Uint8List _screenshot;
  ScreenshotController screenshotController = ScreenshotController();
  List<dynamic> homeTeamLineups = [];
  List<dynamic> awayTeamLineups = [];
  bool isInited = false;
  bool isHomeTeamFavourite = false;
  bool isAwayTeamFavourite = false;
  DataProvider provider;

  @override
  void initState() {
    super.initState();
    provider = DataProvider(database: DBProvider.db, connector: APIConnector());
    getLineupsAsync();
  }

  @override
  Widget build(BuildContext context) {
    if (!isInited)
      getFavouriteState(widget.fixture.homeTeam["id"], widget.fixture.awayTeam["id"]);
      return Scaffold(
          appBar: AppBar(
            title: Text("Fixture details"),
            centerTitle: true,
            actions: <Widget>[
              IconButton(icon: Icon(Icons.share), onPressed: shareData),
            ],
          ),
          body: Screenshot(
            controller: screenshotController,
            child: Container(
              color: Colors.white,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 40,
                    padding: EdgeInsets.fromLTRB(10, 0, 15, 0),
                    color: Color.fromARGB(255, 232, 232, 232),
                    child: SizedBox.expand(
                      child: TextButton(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(widget.fixture.league["name"] + ": " + widget.fixture.league["round"] + " >",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                            ),
                          ),
                        ),
                        onPressed: (){
                          Navigator.push(context, MaterialPageRoute( builder: (context) => StandingPage(leagueId: widget.fixture.leagueId, provider: provider)));
                        },
                      ),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                  getBasicInfo(widget.fixture),
                  Text("Lineups"),
                  ((){
                    if(homeTeamLineups != null && awayTeamLineups != null){
                      if(homeTeamLineups.length >0 && awayTeamLineups.length > 0)
                      return Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: ListView(
                              scrollDirection: Axis.vertical,
                              children: getLineups()),
                        ),
                      );
                      else return Text("No lineups available");
                    }
                    else return Text("No lineups available");
                  }()),
                ],
              ),
            ),
          )
      );
  }

  Widget getBasicInfo(Fixture fixture) {
    final DateFormat formatter = DateFormat('yyyy-MM-dd hh:mm');
    return Container(
      height: 100,
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(color: Colors.black26),
      )),
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: IconButton(
                icon: Icon(isHomeTeamFavourite
                    ? Icons.favorite
                    : Icons.favorite_border),
                onPressed: () =>
                    ChangeHomeTeamFavouriteState(fixture.homeTeam["id"]),
              ),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: CachedNetworkImage(
                    imageUrl: fixture.homeTeam["logo"], //TODO set image url
                    errorWidget: (context, path, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 4,
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        formatter.format(DateTime.parse(fixture.datetime)),
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(5),
                        child: () {
                          String text ="";
                          if(fixture.homeTeam != null && fixture.awayGoals != null)
                            text = fixture.homeGoals.toString() +  "  -  " + fixture.awayGoals.toString();
                          else text = "  -  ";
                            return Text(
                              text,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                            ),
                          );
                        } (),
                      ),
                      Text(
                        fixture.status["long"] + "  " +  (){
                          return fixture.status["elapsed"]!= null? fixture.status["elapsed"].toString() : " ";
                        }(),
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 3,
              child: Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.all(5),
                  child: CachedNetworkImage(
                    imageUrl: fixture.awayTeam["logo"],
                    errorWidget: (context, path, error) => Icon(Icons.error),
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.center,
                child: IconButton(
                  icon: Icon(isAwayTeamFavourite
                      ? Icons.favorite
                      : Icons.favorite_border),
                  onPressed: () =>
                      ChangeAwayTeamFavouriteState(fixture.awayTeam["id"]),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Row> getLineups() {
    List<Row> lineups = [];
    if(homeTeamLineups != null && awayTeamLineups != null && homeTeamLineups.length !=0 && awayTeamLineups.length!= 0) {
      for (int i = 0; i < homeTeamLineups.length; ++i) {
        lineups.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(homeTeamLineups[i]["player"]["number"].toString() +
                    " " +
                    homeTeamLineups[i]["player"]["name"] +
                    " (" +
                    homeTeamLineups[i]["player"]["pos"] +
                    ")"),
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.centerRight,
                child: Text("(" +
                    awayTeamLineups[i]["player"]["pos"] +
                    ") " +
                    awayTeamLineups[i]["player"]["name"] +
                    " " +
                    awayTeamLineups[i]["player"]["number"].toString()),
              ),
            ),
          ],
        ));
      }
      return lineups;
    }
  }

  getLineupsAsync() async {
    var lineups = await getLineupsData();
    if (lineups != null &&  lineups.length >=2 ) {
      setState(() {
        homeTeamLineups = lineups[0]["startXI"] as List;
        awayTeamLineups = lineups[1]["startXI"] as List;
      });
    }
  }

  Future<dynamic> getLineupsData() async {
    //var dataProvider = DataProvider(database: DBProvider.db, connector: APIConnector());
    var lineups = await provider.getFixtureLineups(widget.fixture.id);
    if (lineups == null)
      Catcher.reportCheckedError(
          new Exception("no data received"), StackTrace.current);
    return lineups;
  }

  /*getFixtureAsync() async{
    Future.delayed(Duration(microseconds: 10));
    var fixtureRead = await getFixture();
    setState(() {
      fixture = fixtureRead;
    });
  }*/
  /*Future<Fixture> getFixture() async {
    //var dataProvider = DataProvider(database: DBProvider.db, connector: APIConnector());
    var fixture = await provider.getFixture(widget.id);
    return fixture;
  }*/

  shareData() async {
    try {
      String filepath = (await getTemporaryDirectory()).path;
      String filename =
          DateTime.now().microsecondsSinceEpoch.toString() + ".png";
      await screenshotController.captureAndSave(filepath,
          pixelRatio: 1, fileName: filename);
      File file = File(filepath + "/" + filename);
      if (await file.exists())
        Share.shareFiles([filepath + "/" + filename], text: "match info");
      else
        throw Exception(
            "no such file on directory " + filepath + "/" + filename);
    } catch (e) {
      Catcher.reportCheckedError(e, StackTrace.current);
    }
  }

  void ChangeHomeTeamFavouriteState(int id) {
    bool isFavourite = isHomeTeamFavourite ? false : true;
    if (isFavourite) {
      provider.addTeam(id);
    } else {
      provider.removeFromFavourite(id);
    }
    setState(() {
      isHomeTeamFavourite = isFavourite;
    });
  }

  ChangeAwayTeamFavouriteState(int id) {
    bool isFavourite = isAwayTeamFavourite ? false : true;
    if (isFavourite) {
      provider.addTeam(id);
    } else {
      provider.removeFromFavourite(id);
    }
    setState(() {
      isAwayTeamFavourite = isFavourite;
    });
  }

  void getFavouriteState(int homeId, int awayId) async {
    isInited = true;
    bool homeTeamState = await provider.isFavouriteTeam(homeId);
    bool awayTeamState = await provider.isFavouriteTeam(awayId);
    setState(() {
      isHomeTeamFavourite = homeTeamState;
      isAwayTeamFavourite = awayTeamState;
    });
  }
}
