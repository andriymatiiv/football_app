import 'package:connectivity/connectivity.dart';
import 'package:football_data_app/src/model/fixture_league_model.dart';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

class LeaguesPageWidget extends StatefulWidget {
  DataProvider reader;

  LeaguesPageWidget({Key key, @required this.reader, @required this.onPush})
      : super(key: key);
  final Function(int, String) onPush;

  @override
  _LeaguesPageWidgetState createState() => _LeaguesPageWidgetState();
}

class _LeaguesPageWidgetState extends State<LeaguesPageWidget> {
  var isLoading = false;
  List<FixtureLeagueData> leagues = [];
  List<int> fixturesCount;
  @override
  void initState() {
    super.initState();
    getLeagues();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Leagues data"),
        centerTitle: true,
        actions: <Widget>[
          Container(
            child: Column(
              children: [
                IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () async {
                    await _reloadData();
                  },
                ),
              ],
            ),
          )
        ],
      ),
      body: RefreshIndicator(
        onRefresh: getLeagues,
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : getLeaguesList(),
      ),
    );
  }

  Widget getLeaguesList() {
    return ListView.builder(
        itemCount: leagues.length * 2,
        itemBuilder: (BuildContext context, int index) {
          if (index.isOdd) return Divider();
          int i = index ~/ 2;
          var item = leagues[i];
          return ListTile(
            leading: getIcon(item.logo),
            title: Text(item.name),
            subtitle: Text(item.round),
            onTap: () {
              widget.onPush(item.id, item.name);
            },
            trailing: Container(
              width: 20,
              height: 20,

              decoration: BoxDecoration(
                border: Border.all(color: Colors.black38),
                color: Colors.white60,
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(fixturesCount!=null? fixturesCount[i].toString() : "-"),
              ) ,
            ),
          );
        });
  }
  _reloadData() async {
    setState(() {
      isLoading = true;
    });
    await widget.reader.reloadData();
    await getLeagues();
  }

  Future<void> getLeagues() async {
    var leagues = await widget.reader.getFixtureLeagues();
    var counts = await widget.reader.getFixturesCount();
    setState(() {
      isLoading = false;
      this.leagues = leagues;
      this.fixturesCount = counts;
    });
  }

  Widget getIcon(String path) {
      return CachedNetworkImage(
        imageUrl: path,
        width: 30,
        height: 30,
        errorWidget: (context, path, error) => Icon(Icons.error),
      );
  }
}
