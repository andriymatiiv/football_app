import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:connectivity/connectivity.dart';
class CustomListTile extends StatelessWidget {
  final Fixture fixture;
  final VoidCallback onTapCallback;
  CustomListTile({Key key, @required this.fixture, this.onTapCallback}) :super(key: key);
  @override
  Widget build(BuildContext context) {
    var connection = new Connectivity().checkConnectivity();
    return GestureDetector(
      onTap: onTapCallback,
          child: Container(
            padding: EdgeInsets.fromLTRB(10, 1, 10, 1),
            height: 45,
            child: Row (
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            getIcon(fixture.homeTeam["logo"]),
                            Padding(
                                padding: EdgeInsets.all(2),
                                child: Text(fixture.homeTeam["name"], style: _getFontStyle()),
                            )

                          ],
                        ),
                        Row(
                          children: <Widget>[
                              getIcon(fixture.awayTeam["logo"]),
                              Padding(
                                padding: EdgeInsets.all(2),
                                child: Text(fixture.awayTeam["name"], style: _getFontStyle()),
                              )
                          ],
                        ),
                      ]
                  ),
                ),
                Expanded(
                    flex: 2,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                          child: (){
                          String text = "no status";
                          if(fixture.status["elapsed"]!= null && (fixture.status["short"] != "FT" && fixture.status["short"] != "AET" && fixture.status["short"] != "PEN") ) {
                            text = fixture.status["elapsed"].toString();
                          }
                          else{
                            text = fixture.status["short"];
                          }
                          return Text(text,
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.w300
                            ),
                            );
                          }()
                        ),
                      ],
                    )
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                        left: BorderSide(color: Colors.black26),
                      )
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(padding: EdgeInsets.all(2),
                          child: Text(fixture.homeGoals!=null ? fixture.homeGoals.toString() : "-", style: _getFontStyle(),),
                        ),
                        Padding(padding: EdgeInsets.all(2),
                            child: Text(fixture.awayGoals!=null ? fixture.awayGoals.toString() : "-", style: _getFontStyle(),)
                        )
                      ],
                    ),
                  )
                ),
              ],
            ),
          )

    );
  }
  Widget getIcon(String path){
    var connection = new Connectivity().checkConnectivity();
    if(!(connection == ConnectivityResult.wifi || connection == ConnectivityResult.mobile))
      return CachedNetworkImage(
        imageUrl:path,
        errorWidget: (context, path, error) => Icon(Icons.error),
        width: 20,
        height: 20);
    else return Icon(Icons.image_outlined, color: Colors.black26, size: 20,);
  }
  TextStyle _getFontStyle(){
    return TextStyle(
      fontWeight: FontWeight.normal,
      fontSize: 14
    );
  }
}
