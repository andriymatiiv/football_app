import 'package:football_data_app/src/views/app.dart';
import 'package:flutter/material.dart';

class OwnBottomNavigationBar extends StatefulWidget {
  final PageId currentPage;
  final ValueChanged<PageId> pageSelected;
  OwnBottomNavigationBar({@required this.currentPage, @required this.pageSelected});
  @override
  _OwnBottomNavigationBarState createState() => _OwnBottomNavigationBarState();
}

class _OwnBottomNavigationBarState extends State<OwnBottomNavigationBar> {
  int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          widget.pageSelected(PageId.values[index]);
          setState(() {
            _currentIndex = index;
          });
        },
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black38,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
              icon: Opacity(
                opacity: isSelected(PageId.main),
                child: Icon(
                  Icons.sports_soccer,
                ),
              ),
              label: "Fixtures"
          ),
          BottomNavigationBarItem(
              icon: Opacity(
                opacity: isSelected(PageId.live),
                child: Icon(
                    Icons.online_prediction
                ),
              ),
              label: "Live"
          ),
          BottomNavigationBarItem(
              icon: Opacity(
                opacity: isSelected(PageId.favourite),
                child: Icon(
                    Icons.favorite_border
                ),
              ),
              label: "Favourite"
          ),
          BottomNavigationBarItem(
              icon: Opacity(
                opacity: isSelected(PageId.table),
                child: Icon(
                    Icons.emoji_events_outlined
                ),
              ),
              label: "Tables"
          )
        ]
    );
  }
  double isSelected(PageId id){
    return 1;//id == currentPage? 1 : 0.5;
  }
}
