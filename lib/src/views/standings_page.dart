import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:football_data_app/src/providers/data_provider.dart';

class StandingPage extends StatefulWidget {
  final int leagueId;
  final DataProvider provider;

  StandingPage({Key key, @required this.leagueId, @required this.provider})
      : super(key: key);

  @override
  _StandingPageState createState() => _StandingPageState();
}

class _StandingPageState extends State<StandingPage> {
  dynamic standings;
  List<dynamic> standingsList;
  bool isLoaded = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    () async {
      standings = await widget.provider.getStandings(widget.leagueId);
      setState(() {
        if(standings != null)
          standingsList = standings["standings"] as List;
        isLoaded = true;
      });

    }();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Standings"),
          centerTitle: true,
        ),
        body: () {
          if (!isLoaded) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (standings == null) {
            return Center(
              child: Text("No standings avaliable"),
            );
          }
          return SingleChildScrollView(
            physics: ScrollPhysics(),
            child: Column(
              children: [
                Container(
                  height: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                          flex: 2,
                          child: CachedNetworkImage(
                            imageUrl: standings["logo"],
                            width: 50,
                            height: 50,
                            errorWidget: (context, path, error) => Icon(Icons.error),
                          )
                      ),
                      Expanded(
                          flex: 3,
                          child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Text(
                              standings["name"],
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 24,
                              ),
                            ),
                          )
                      ),
                    ],
                  ),
                ),
                /* Expanded(
                    child: getStandingsList(standingsList[0])
                ),*/
                ...standingsList.map((e) => getGroup(e)).toList(),
              ],
            ),
          );
        }());
  }
  Widget getGroup(dynamic group){
    return Column(
      children: [
        Container(
          color: Color.fromARGB(1, 242, 242, 242),
          child: Padding(
            padding: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0),
              child: Text(group[0]["group"].toString()),
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 0),
          child: getHeader(),
        ),
        getStandingsList(group),
      ],
    );
  }
  Widget getStandingsList(dynamic standings){
   return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemCount: standings.length,
        itemBuilder: (context, index) {
          var standing = standings[index];
          return Container(
            height: 50,
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(color: Colors.black38))),
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(standing["rank"].toString()),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: getIcon(standing["team"]["logo"]),
                  ), //logo
                  Expanded(
                    flex: 4,
                    child: Text(standing["team"]["name"]),
                  ), //team name
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(standing["all"]["played"].toString()),
                    ),
                  ), //team matches
                  Expanded(
                    flex: 2,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                          standing["all"]["goals"]["for"].toString() +
                              " : " +
                              standing["all"]["goals"]["against"]
                                  .toString()),
                    ),
                  ), //team goals (+-)
                  Expanded(
                    flex: 1,
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(standing["points"].toString()),
                    ),
                  ), //team points
                ],
              ),
            ),
          );
        });
  }
  Widget getHeader(){
    return Container(
      height: 40,
      decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(color: Colors.black38))),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 5.0),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.center,
                child: Text("# "),
              ),
            ),
             //logo
            Expanded(
              flex: 5,
              child: Text("Team name"),
            ), //team name
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.center,
                child: Text("G"),
              )
              ,
            ), //team matches
            Expanded(
              flex: 2,
              child: Align(
                alignment: Alignment.center,
                child: Text("F:A"),
              ),
            ), //team goals (+-)
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.center,
                child: Text("P"),
              ),
            ), //team points
          ],
        ),
      ),
    );
  }
  Widget getIcon(String path) {
    return CachedNetworkImage(
      imageUrl: path,
      width: 20,
      height: 20,
      errorWidget: (context, path, error) => Icon(Icons.error),
    );
  }
}
