import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/views/custom_tile.dart';

class LiveFixturesPage extends StatefulWidget {
  final Function (String, Fixture) onPush;
  LiveFixturesPage({Key key, @required this.onPush}) : super (key: key);
  @override
  _LiveFixturesPageState createState() => _LiveFixturesPageState();
}

class _LiveFixturesPageState extends State<LiveFixturesPage> {
  List<Fixture> activeFixtures;
  DataProvider provider;
  bool isLoaded = false;
  @override
  void initState() {
    super.initState();
    provider = DataProvider(database: DBProvider.db, connector: APIConnector());
    /*() async {

      var data = await provider.getActiveFixtures();
      setState(() {
        activeFixtures = data;
        isLoaded = true;
      });
    }();*/
    loadData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Active fixtures"),
        centerTitle: true,
      ),
      body: () {
        if(!isLoaded){
          return Center(child: CircularProgressIndicator(),);
        }
        else{
          if(activeFixtures == null || activeFixtures.length == 0)
            return Center( child: Text("No fixtures are active now"));
          else{
            return RefreshIndicator(
                child: ListView.builder(
                  itemCount: activeFixtures.length*2,
                  itemBuilder: (BuildContext context, int index) {
                    if(index.isOdd)
                      return Divider();
                    int i = index ~/2;
                    return CustomListTile(
                      fixture: activeFixtures[i],
                      onTapCallback: () => widget.onPush("/fixture", activeFixtures[i]),
                    );
                  },
                ),
                onRefresh: loadData);


          }
        }
      }(),
    );
  }

  Future<void> loadData() async {
    var data = await provider.getActiveFixtures();
    setState(() {
      activeFixtures = data;
      isLoaded = true;
    });
  }
}
