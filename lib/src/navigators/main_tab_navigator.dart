import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/views/fixture_page.dart';
import 'package:football_data_app/src/views/fixtures_leagues_page.dart';
import 'package:football_data_app/src/views/league_fixtures_page.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
class MainPageTabNavigator extends StatelessWidget {

  final String root = "/";
  final String leagueFixturesRoad = "/league_fixtures";
  final String fixtureRoad = "/league_fixtures/fixture";
  final GlobalKey<NavigatorState> navigatorKey;
  MainPageTabNavigator({this.navigatorKey});

  void _push(BuildContext context, {int id, String name, Fixture fixture})  {
    var routes =  _getRoutes(context, id: id, name: name, fixture: fixture);
    if(id == null && name == null)
      Navigator.push(context, MaterialPageRoute(builder: (context) => routes[root](context)));
    else if(fixture != null)
      Navigator.push(context, MaterialPageRoute(builder: (context) => routes[fixtureRoad](context)));
    else if(id != null && name !=null)
      Navigator.push(context, MaterialPageRoute(builder: (context) => routes[leagueFixturesRoad](context)));
  }
  Map<String, WidgetBuilder> _getRoutes(BuildContext context, {int id, String name, Fixture fixture}) {
    var dataProvider = new DataProvider(database: DBProvider.db, connector: APIConnector());
    Map<String, WidgetBuilder> routes = {
      root: (context) => LeaguesPageWidget(
        reader: dataProvider,
        onPush:(id,name) => _push(context, id: id, name: name),
      ),
      leagueFixturesRoad: (context)=> LeagueFixturesWidget(
          leagueId: id,
          leagueName: name,
          reader: dataProvider,
          onPush: (id, name, fixture) => _push(context, id: id, name: name, fixture: fixture),
      ),
      fixtureRoad: (context) => FixtureInfo(
          fixture: fixture,
      ),
    };
    return routes;
  }
  @override
  Widget build(BuildContext context){
    var routes =  _getRoutes(context);
    return Navigator(
      key: navigatorKey,
      initialRoute: root,
      onGenerateRoute: (routeSettings) {
        return MaterialPageRoute(
          builder: (context) => routes[routeSettings.name](context),
        );
      },
    );
  }
}
