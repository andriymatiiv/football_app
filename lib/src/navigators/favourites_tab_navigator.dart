import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/views/fixture_page.dart';
import 'package:football_data_app/src/views/league_fixtures_page.dart';
import 'package:football_data_app/src/views/team_fixtures_page.dart';

import '../views/favourite_teams_page.dart';

enum RoutesEnum {rootRoad , fixturesRoad, fixtureRoad}

class FavouritesPageNavigator extends StatelessWidget {

  final String rootRoadName = "/";
  final String fixturesRoadName = "/fixtures";
  final String fixtureRoadName = "/fixtures/fixture";
  final GlobalKey<NavigatorState> navigatorKey;
  FavouritesPageNavigator({this.navigatorKey});

  void _push(BuildContext context, {@required int id, @required RoutesEnum route, Fixture fixture}){
      var roads = getRoutes(context, id: id, fixture: fixture);
      Navigator.push(context, MaterialPageRoute(builder: (context)=>roads[route](context)));
  }
  Map<RoutesEnum, WidgetBuilder> getRoutes(BuildContext context, {int id, RoutesEnum route, Fixture fixture}){
    return {
      RoutesEnum.rootRoad : (context)=> FavouriteTeamsPage(onPush: (id, road) => _push(context, id: id, route: road )),
      RoutesEnum.fixturesRoad : (context) => TeamFixturesPage(
          teamId: id,
          provider: DataProvider(connector: APIConnector(), database: DBProvider.db),
          onPush: ( fixture, route, id) => _push(context, id: id, route: route, fixture: fixture)),
      RoutesEnum.fixtureRoad : (context) => FixtureInfo(fixture: fixture)
    };
  }
  @override
  Widget build(BuildContext context) {
    var routes =  getRoutes(context);
    return Navigator(
      initialRoute: rootRoadName,
      key: navigatorKey,
      onGenerateRoute: (routeSettings) {
        RoutesEnum route;
        if(routeSettings.name == "/")
          route = RoutesEnum.rootRoad;
        else if(routeSettings.name == "/fixtures")
        route = RoutesEnum.fixturesRoad;
        else route = RoutesEnum.fixtureRoad;
        return MaterialPageRoute(builder: (context) => routes[route](context));
      },
    );
  }
}
