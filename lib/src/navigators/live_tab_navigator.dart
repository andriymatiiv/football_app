import 'package:flutter/material.dart';
import 'package:football_data_app/src/model/fixture_model.dart';
import 'package:football_data_app/src/views/fixture_page.dart';
import 'package:football_data_app/src/views/live_fixtures_page.dart';

final String root = "/";
final String fixtureRoad = "/fixture";
class LivePageNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  LivePageNavigator({@required this.navigatorKey});

  void _push(BuildContext context, {String path, Fixture fixture}){
      var routes= getRoutes(context, fixture: fixture);
      Navigator.push(context, MaterialPageRoute(builder: (context) => routes[path](context)));
  }
  Map<String, WidgetBuilder> getRoutes(BuildContext context, {Fixture fixture}){
    return {
      "/" : (context) => LiveFixturesPage(
        onPush: (path, fixture) => _push(context, path: path, fixture: fixture),
      ),
      "/fixture" : (context) => FixtureInfo(fixture: fixture)
    };
  }
  @override
  Widget build(BuildContext context) {
    var routes = getRoutes(context);
    return Navigator(
      key: navigatorKey,
      initialRoute: root,
      onGenerateRoute: (routeSettings){
        return MaterialPageRoute(builder: (context) => routes[routeSettings.name](context));
      },
    );
  }
}
