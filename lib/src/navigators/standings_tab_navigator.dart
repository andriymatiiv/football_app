import 'package:flutter/material.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/data_provider.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/views/leagues_page.dart';
import 'package:football_data_app/src/views/standings_page.dart';
final String root = "/";
final String leagueStandingsPage = "/standings";
class StandingsTabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  StandingsTabNavigator({@required this.navigatorKey});

  void _push(BuildContext context, {String route, int id}){
    var routes = getRoutes(context, id: id);
    Navigator.push(context, MaterialPageRoute(builder: (context) => routes[route](context)));
  }
  Map<String, WidgetBuilder> getRoutes(BuildContext context, {int id}){
    return {
      root : (context) => LeaguesPage(
        provider: DataProvider(connector: APIConnector(), database: DBProvider.db),
        onPush: (route, id) => _push(context, route: route, id: id),
      ),
      leagueStandingsPage : (context) => StandingPage(
          provider: DataProvider(connector: APIConnector(), database: DBProvider.db),
          leagueId: id)
    };
  }
  @override
  Widget build(BuildContext context) {
    var routes= getRoutes(context);
    return Navigator(
      key: navigatorKey,
      initialRoute: root,
      onGenerateRoute: (routeSettings){
        return MaterialPageRoute(builder: (context) => routes[routeSettings.name](context));
      },
    );
  }
}
