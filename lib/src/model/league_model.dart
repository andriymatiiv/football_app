class League{
  int id;
  String name;
  String logo;
  String country;
  String flag;
  int season;
  String start;
  String end;
  League({this.id,
    this.name,
    this.country,
    this.logo,
    this.flag,
    this.season,
    this.start,
    this.end
  });
  factory League.fromJson (Map<String, dynamic> jsonData){
    return League(
      id: jsonData["id"],
      name: jsonData["name"],
      logo:  jsonData["logo"],
      country: jsonData["country"],
      flag:  jsonData["flag"],
      season: jsonData["season"],
      start: jsonData["start"],
      end: jsonData["end"],
    );
  }
  Map<String, dynamic> toJson() =>{
    "id" : id.toString(),
    "name" : name,
    "logo" : logo,
    "country" : country,
    "flag" : flag,
    "season" : season.toString(),
    "start" : start,
    "end" : end
  };
}