class FixtureLeagueData{
  int id;
  String name;
  String country;
  String logo;
  String flag;
  String season;
  String round;
  String datetime;
  FixtureLeagueData({this.id,
    this.name,
    this.country,
    this.logo,
    this.flag,
    this.season,
    this.round,
    this.datetime
  });

  factory FixtureLeagueData.fromJson(Map<String, dynamic> parsedJson){
    int currId = 0;
    if(parsedJson["id"] is int){
      currId = parsedJson["id"];
    }
    else {
      currId = int.parse(parsedJson["id"]);
    }
    return FixtureLeagueData(
      name: parsedJson["name"],
      id: currId,
      country: parsedJson["country"],
      logo: parsedJson["logo"],
      flag: parsedJson["flag"],
      season: parsedJson["season"].toString(),
      round: parsedJson["round"]
    );
  }
  Map<String, dynamic> toJson() =>{
    "id" : id.toString(),
    "name" : name,
    "country" : country,
    "logo" : logo,
    "flag" : flag,
    "season" : season,
    "round" : round,
    "datetime" : DateTime.now().toIso8601String()
  };
}

