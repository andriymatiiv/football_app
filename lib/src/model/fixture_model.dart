import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DBRecordModel{
  int id;
  String fixtureData;
  String datetime;
  DBRecordModel({@required this.id, @required this.fixtureData, @required this.datetime});

  Map<String, dynamic> toJson() => {
    "id" : id.toString(),
    "fixtureData": fixtureData,
    "datetime": datetime
  };
  factory DBRecordModel.fromJson(Map<String, dynamic> jsonData){
    return new DBRecordModel(
        id: jsonData["id"],
        fixtureData: jsonData["fixtureData"],
        datetime: jsonData["datetime"]);
  }
}

class Fixture{
  int id;
  int leagueId;
  Map<String, dynamic> league;
  Map<String, dynamic> homeTeam;
  Map<String, dynamic> awayTeam;
  Map<String, dynamic> venue;
  Map<String, dynamic> status;
  int homeGoals;
  int awayGoals;
  String datetime;
  Fixture({
    this.id,
    this.status,
    this.leagueId,
    this.homeTeam,
    this.awayTeam,
    this.homeGoals,
    this.awayGoals,
    this.datetime,
    this.venue,
    this.league
  });

  factory Fixture.fromJson(Map<String, dynamic> jsonFixture){
    return new Fixture(
      id: jsonFixture["fixture"]["id"],
      datetime: jsonFixture["fixture"]["date"],
      status: jsonFixture["fixture"]["status"],
      leagueId: jsonFixture["league"]["id"],
      homeTeam: jsonFixture["teams"]["home"],
      awayTeam: jsonFixture["teams"]["away"],
      awayGoals: jsonFixture["goals"]["away"],
      homeGoals: jsonFixture["goals"]["home"],
      venue: jsonFixture["fixture"]["venue"],
      league: jsonFixture["league"]
    );
  }
}