import 'package:flutter/material.dart';

class Team{
  int id;
  String name;
  String country;
  int founded;
  String logo;
  Team({this.id, this.name, this.logo,this.country, this.founded});

  factory Team.fromJson(Map<String, dynamic> jsonValue){
    return Team(
      id: jsonValue["id"],
      name: jsonValue["name"],
      logo:  jsonValue["logo"],
      country: jsonValue["country"],
      founded: jsonValue["founded"]
    );
  }

  Map<String, dynamic> toJson() => {
    "id" : id.toString(),
    "name": name,
    "country": country,
    "founded": founded.toString(),
    "logo": logo
  };
}