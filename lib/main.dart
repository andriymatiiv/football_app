import 'dart:async';

import 'package:catcher/catcher.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:football_data_app/src/providers/api_connector.dart';
import 'package:football_data_app/src/providers/db_provider.dart';
import 'package:football_data_app/src/providers/service.dart';
import 'package:football_data_app/src/views/app.dart';
import 'package:football_data_app/src/model/error_messaging.dart';
import 'package:workmanager/workmanager.dart';

final String taskName = "backgroundTask";
ServiceRoutine routine;
NotificationAppLaunchDetails notificationAppLaunchDetails;
final navKey = Catcher.navigatorKey;
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin= FlutterLocalNotificationsPlugin();
void main() async {
  EmailInfo info = EmailInfo();
  CatcherOptions debugOptions = CatcherOptions(SilentReportMode(), [ConsoleHandler(),
        EmailAutoHandler('smtp.gmail.com', 587, info.sender, info.senderName, info.password, [info.receiver],
        emailTitle: info.emailTitle)
      ]);
  CatcherOptions releaseOptions = CatcherOptions(DialogReportMode(), [
        EmailAutoHandler('smtp.gmail.com', 587, info.sender, info.senderName, info.password, [info.receiver],
          emailTitle: info.emailTitle)
      ]);
  Catcher(
      runAppFunction: startApp,
      debugConfig: debugOptions,
      releaseConfig: releaseOptions,
      ensureInitialized: true);
}

void startApp() async {
  notificationAppLaunchDetails = await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();
  FlutterError.onError = (FlutterErrorDetails details) async {Catcher.reportCheckedError(details, details.stack);};
  routine = new ServiceRoutine(connector: new APIConnector());
  Workmanager.initialize(callbackDispatcher, isInDebugMode: false);
  initializeWorkmanager();
  routine.initializeNotifications();
  var connection =  await Connectivity().checkConnectivity();
  if (connection == ConnectivityResult.mobile || connection == ConnectivityResult.wifi) {
    bool isActual = await DBProvider.db.isActualDataStored(5);
    if (!isActual)
      routine.executeTask();
  }
  runApp(MainWidget());
}

class MainWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: Catcher.navigatorKey,
      debugShowCheckedModeBanner: false,
      title: "Football data app",
      home: App(),
    );
  }
}

void initializeWorkmanager() {
  Workmanager.registerPeriodicTask("1", taskName,
      frequency: Duration(minutes: 30),
      constraints: Constraints(
        networkType: NetworkType.connected,
      ));
}

Future<void> callbackDispatcher() async {
  Workmanager.executeTask((task, inputData) async {
    bool isOk = true;
    if (task == taskName) {
      isOk =false;
      try{
        isOk = await ServiceRoutine(connector: APIConnector()).executeTask();
      }
      catch (e){
        Catcher.reportCheckedError(e, StackTrace.current);
      }
    }
    return true;
  });
}
